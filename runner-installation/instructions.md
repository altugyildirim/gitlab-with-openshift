### OpenShift Operator Installation ###

1. Login OpenShift --> Click Operators, then OperatorHub

2. below All Items, search  <b> GitLab Runner </b>

3. Click GitLab Runner Operator

4. You can find official docs on the summary page, then click <b> Install </b>

5. Choose:
-    Update Channel: <b> stable </b>
-    Installation Mode: <b> A specific namespace on the cluster </b>
-    Installed Namespace: <b> xxx </b>
-    Approval Strategy: <b> Automatic </b>
-    Click <b> Install </b>

### GitLab Runner Installation on OpenShift ###

1. Login GitLab --> Click <i> your desired project </i>

2. Go <b> Settings </b> --> Click <b> CI/CD </b>

3. Get <b> registration token </b>

4. Get <b> URL </b>

5. Insert token to gitlab-runner-secret.yaml as shown in runner-installation folder

6. Insert URL to gitlab-runner.yaml as shown in runner-installation folder

7. Apply both of scripts

